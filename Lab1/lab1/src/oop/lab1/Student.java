package oop.lab1;
 
//TODO Write class Javadoc
/**
 * A simple model for a student with ID and name and it inherit from Person class
 */
public class Student extends Person {
        /** The student's ID*/
        private long id;
       
        //TODO Write constructor Javadoc
        /**
         * Initialize a new student object
         * @param name is the name of the new student
         * @param id is the ID of the new student
         */
        public Student(String name, long id) {
                super(name); // name is managed by Person
                this.id = id;
        }
 
        /** return a string representation of this Student. */
        public String toString() {
                return String.format("Student %s (%d)", getName(), id);
        }
 
        //TODO Write equals
        /**
         * Compare students by ID
         * They are equal if their IDs match
         * @return true if their IDs match, otherwise return false
         * */
        public boolean equals(Object obj) {
                if(obj == null)
                        return false;
                if(obj.getClass() != this.getClass())
                        return false;
                Student other = (Student) obj;
                if(this.id == other.id)
                        return true;
                return false;
        }
}