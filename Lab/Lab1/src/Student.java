
//TODO Write class Javadoc
/**
 * A simple model for a student inherit from person and add id.
 * 
 * @author Natanon Poonawagul
 */
public class Student extends Person {
	/** the student's ID */
	private long id;
	
	//TODO Write constructor Javadoc

	/**
	 * Initialize a new Person object.
	 * @param name is the name of the new Student
	 * @param id is the ID of the new Student
	 */
	
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Compare student's by both name and ID.
	 * They are equal if the ID matches.
	 * @param other is another Student to compare to this one.
	 * @return true if ID matches, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj.getClass() != this.getClass())
			return false;
		Student other = (Student) obj;
		if(id == other.id)
			return true;
		return false;
	}
}
